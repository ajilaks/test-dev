import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';

class Header extends React.Component{
constructor(){
super()
this.handleChange = this.handleChange.bind(this);
this.handleNameChange = this.handleNameChange.bind(this);
this.state = {
nama:'',
currentNama:[],
name:'',
email:'',
telp:'',
nameWarning:'',
nameNotValid:false

}

}   

addName(){
    var str = this.state.nama;
    var res = str.split("");
    var result=[];
    
    for(var i =0; i < str.length ; i++){
        if(result.indexOf(res[i]) === -1) result.push(res[i]);
    }
    this.setState({
        currentNama: result.join(" ")
         
        })
// var tambahNama = this.state.currentNama
// tambahNama.push(this.state.nama)
// this.setState({
// currentNama: tambahNama
 
// })


}

handleChange(event) {
 
    this.setState({[event.target.name] : event.target.value })
  }

  handleNameChange(event) {
    this.setState({nameWarning : '' })
    this.setState({nameNotValid : false })
    this.setState({[event.target.name] : event.target.value })
  }
  submitForm(){
    var str = this.state.name;
    var res = str.split("");
    if(res.length<3){
        this.setState({nameWarning : "Min 3" }) 
        this.setState({nameNotValid : true })
    }

  }

  validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
        return true;
    }
};
render(){
return(
<div>
    <div>
    <h1>Nomor 1</h1> 
    <center>
    <div style={{width: '25'+'%',height:'300'+'px'}}>
        <div style={{width: '25'+'%',height: '70'+'%', backgroundColor:'yellow', float: 'right'}}></div>
        <div style={{width: '100'+'%',height: '20'+'%', backgroundColor:'blue'}}></div> 
        <div style={{width: '25'+'%',height: '70'+'%', backgroundColor:'red', float: 'left'}}></div>
        <div style={{width: '50'+'%',height: '50'+'%'}}></div>
        <div style={{width: '100'+'%',height: '20'+'%', backgroundColor:'green'}}></div>
    </div>
    </center>
    </div>
    <div>
    <h1>Nomor 2</h1>
   
    <input name="nama" type="text" onChange={this.handleChange}/>
    <input name="submit" value="Add Student" type="submit" onClick={() =>this.addName()}/>
    <h2>Result:</h2>
    {this.state.nama}<br/>
    {this.state.currentNama}
    </div>

    <div>
    <h1>Nomor 3</h1>
    <form>
    <b>Nama</b>
    <div>
        
    <input required name="name" type="text" onChange={this.handleNameChange}/>{this.state.nameWarning}
    </div>
    <b>E-mail</b>
    <div>
    <input required name="email" type="email" onChange={this.handleChange}/>
    </div>
    <b>No. Telp</b>
    <div>
    <input required name="telp" type="tel" onChange={this.handleChange}/>
    </div>
    <br/>
    <div>
    <input disabled={this.state.nameNotValid} name="formSubmit" value="Submit" type="submit" onClick={() =>this.submitForm()}/>  
    </div>
    </form>
    </div>
    </div>
)

}

}

export default Header